'use strict';

const AWS = require('aws-sdk');
const { v4: uuidv4 } = require("uuid");

const dynamoDb = new AWS.DynamoDB.DocumentClient();
const params = {
  TableName: "ds-recruitment-candidates",
};

module.exports.listCandidates = async (event) => {
  try {
    console.log(event);
    let data = await dynamoDb.scan(params).promise();

    return {
      statusCode: 200,
      body: JSON.stringify(data.Items)
    };
  } catch (err) {
    console.log("Error", err);
    return {
      statusCode: err.statusCode ? err.statusCode : 500,
      body: JSON.stringify({
        error: err.name ? err.name : "Exception",
        message: err.message ? err.message : "Unknown error",
      }),
    };
  };
};


module.exports.getCandidate = async event => {
  try {
    const { candidateId } = event.pathParameters;

    const data = await dynamoDb
      .get({
        ...params,
        Key: {
          candidate_id: candidateId,
        },
      })
      .promise();

    if (!data.Item) {
      return {
        statusCode: 404,
        body: JSON.stringify({ error: "Candidate doesn't exist." }, null, 2),
      };
    }

    const candidate = data.Item;

    return {
      statusCode: 200,
      body: JSON.stringify(candidate, null, 2),
    };
  } catch (err) {
    console.log("Error", err);
    return {
      statusCode: err.statusCode ? err.statusCode : 500,
      body: JSON.stringify({
        error: err.name ? err.name : "Exception",
        message: err.message ? err.message : "Unknown error",
      }),
    };
  }
};

module.exports.postCandidate = async (event) => {
  try {
    const timestamp = new Date().getTime();

    let dados = JSON.parse(event.body);

    const { cand_name, email, cellphone } = dados;

    const candidate = {
      candidate_id: uuidv4(),
      cand_name,
      email,
      cellphone,
      status: true,
      created: timestamp,
      updated: timestamp,
    };

    await dynamoDb
      .put({
        TableName: "ds-recruitment-candidates",
        Item: candidate,
      })
      .promise();

    return {
      statusCode: 201,
    };
  } catch (err) {
    console.log("Error", err);
    return {
      statusCode: err.statusCode ? err.statusCode : 500,
      body: JSON.stringify({
        error: err.name ? err.name : "Exception",
        message: err.message ? err.message : "Unknown error",
      }),
    };
  }
}

module.exports.updateCandidate = async (event) => {
  const { candidateId } = event.pathParameters

  try {
    const new_timestamp = new Date().getTime();

    let data = JSON.parse(event.body);

    const { cand_name, email, cellphone } = data;

    await dynamoDb
    .update({
      ...params,
      Key: {
        candidate_id: candidateId
      },
      UpdateExpression:
      'SET cand_name = :cand_name, email = :email,' +
      'cellphone = :cellphone, updated = :updated',
      ConditionExpression: 'attribute_exists(candidate_id)',
      ExpressionAttributeValues: {
        ':cand_name' : cand_name, 
        ':email' : email,
        ':cellphone' : cellphone,
        ':updated' : new_timestamp
    }
    })
    .promise()

    return {
      statusCode: 204,
    }
  } catch (err) {
    console.log("Error", err);

    let error = err ? err.name : "Exception";
    let message = err.message ? err.message : "Unknown error";
    let statusCode = err.statusCode? err.statusCode : 500;

    if(error == 'ConditionalCheckFailedException') {
      error = "Candidate doesn't exist.";
      message = `Resource with the id ${candidateId} doesn't exists and can't be updated.`
      statusCode = 404;
    }

    return {
      statusCode,
      body: JSON.stringify({
        error,
        message
      })
    }
  }
}

module.exports.deleteCandidate = async (event) => {
  const { candidateId } = event.pathParameters

  try {
    await dynamoDb
    .delete({
      ...params,
      Key: {
        candidate_id: candidateId
      },
      ConditionExpression: 'attribute_exists(candidate_id)'
    })
    .promise()
  
    return {
      statusCode: 204
    }
  } catch (err) {
    console.log("Error", err);

    let error = err ? err.name : "Exception";
    let message = err.message ? err.message : "Unknown error";
    let statusCode = err.statusCode? err.statusCode : 500;

    if(error == 'ConditionalCheckFailedException') {
      error = "Candidate doesn't exist.";
      message = `Resource with the id ${candidateId} doesn't exists and can't be deleted.`
      statusCode = 404;
    }

    return {
      statusCode,
      body: JSON.stringify({
        error,
        message
      })
    }
  }
}